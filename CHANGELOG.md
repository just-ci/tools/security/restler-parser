## [1.7.4](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.7.3...v1.7.4) (2022-09-20)


### Bug Fixes

* explicit path ([4cf6d8d](https://gitlab.com/just-ci/tools/security/restler-runner/commit/4cf6d8db87118dd485b173f4578f0b07ec175239))

## [1.7.3](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.7.2...v1.7.3) (2022-08-31)


### Bug Fixes

* metrics cat ([00c42e5](https://gitlab.com/just-ci/tools/security/restler-runner/commit/00c42e57253a22d376dfb23b9cc51bfc8bbee172))

## [1.7.2](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.7.1...v1.7.2) (2022-08-31)


### Bug Fixes

* separate metrics ([c141dcf](https://gitlab.com/just-ci/tools/security/restler-runner/commit/c141dcf7e257c905e232f2c78b6bf443e96c1472))

## [1.7.1](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.7.0...v1.7.1) (2022-08-31)


### Bug Fixes

* allow fuzz job to fail ([07b77ea](https://gitlab.com/just-ci/tools/security/restler-runner/commit/07b77ea63b7b830172cfee266e040672f8ac9695))

# [1.7.0](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.6.0...v1.7.0) (2022-08-29)


### Features

* allow custom grammar compilation ([c84b1ba](https://gitlab.com/just-ci/tools/security/restler-runner/commit/c84b1bab09c3fc247198cff09f8e732d383b7317))

# [1.6.0](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.5.0...v1.6.0) (2022-08-23)


### Features

* measure resource usage as metrics ([5255501](https://gitlab.com/just-ci/tools/security/restler-runner/commit/5255501154adcc78dc31015db2b29c093a536030))

# [1.5.0](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.4.0...v1.5.0) (2022-08-10)


### Features

* scheduling ([37ba4a0](https://gitlab.com/just-ci/tools/security/restler-runner/commit/37ba4a09ae54bb049985f45cfb69a527553ab70a))

# [1.4.0](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.3.0...v1.4.0) (2022-08-08)


### Features

* error only parsing ([5cec0ec](https://gitlab.com/just-ci/tools/security/restler-runner/commit/5cec0eca759caf8228e52262869984b2604022ce))

# [1.3.0](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.2.5...v1.3.0) (2022-08-03)


### Features

* separate 4xx parsing ([bc0c744](https://gitlab.com/just-ci/tools/security/restler-runner/commit/bc0c7448f52a62e6dcf8f7ddde2036e1d72a3869))

## [1.2.5](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.2.4...v1.2.5) (2022-08-02)


### Bug Fixes

* use new ci in template ([5c8426e](https://gitlab.com/just-ci/tools/security/restler-runner/commit/5c8426e79c468218ec46cb01be028cacb26ef459))

## [1.2.4](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.2.3...v1.2.4) (2022-08-02)


### Bug Fixes

* deprecation ([e26a916](https://gitlab.com/just-ci/tools/security/restler-runner/commit/e26a91675f68968fbb0844b9e02327314cace9e6))

## [1.2.3](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.2.2...v1.2.3) (2022-07-20)


### Bug Fixes

* hardcode report filename ([4efcc22](https://gitlab.com/just-ci/tools/security/restler-runner/commit/4efcc2251d43dce24a9c42d26414525bfba17998))

## [1.2.2](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.2.1...v1.2.2) (2022-07-19)


### Bug Fixes

* always store compile artifacts ([5a4e7b9](https://gitlab.com/just-ci/tools/security/restler-runner/commit/5a4e7b9f7a95804fd4358100fd9f25dc944da522))

## [1.2.1](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.2.0...v1.2.1) (2022-07-19)


### Bug Fixes

* ensure coreutils ([653226b](https://gitlab.com/just-ci/tools/security/restler-runner/commit/653226b8c48de6a575064f5b5a2624c8f18ec22b))

# [1.2.0](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.1.0...v1.2.0) (2022-07-19)


### Features

* timeout ([7d3dc92](https://gitlab.com/just-ci/tools/security/restler-runner/commit/7d3dc92ac57654f278ef8556f365d64259a23541))

# [1.1.0](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.0.1...v1.1.0) (2022-07-18)


### Features

* all status codes ([f40dfd4](https://gitlab.com/just-ci/tools/security/restler-runner/commit/f40dfd447d0f6245f07ffdcae976dff5aae0eec9))

## [1.0.1](https://gitlab.com/just-ci/tools/security/restler-runner/compare/v1.0.0...v1.0.1) (2022-07-05)


### Bug Fixes

* renamed release ([d7c1ac1](https://gitlab.com/just-ci/tools/security/restler-runner/commit/d7c1ac1c672bdd762d361da79e669859c416e209))

# 1.0.0 (2022-03-08)


### Features

* parse more data ([9d31cf1](https://gitlab.com/just-ci/tools/security/restler-runner/commit/9d31cf1689d09b70b24c7afffabde2cc6e4c8542))
